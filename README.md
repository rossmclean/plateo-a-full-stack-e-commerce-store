# Plateo - A Full-Stack E-Commerce Store

This MERN full-stack web application was built using:
-MongoDB.\
-Express.\
-React.\
-NodeJS.\
-NodeMailer.\
-AdminJS.\
-Stripe Payments

This was a challenging yet rewarding application to build. I particularly enjoyed learning how to integrate Stripe payments and building effective administrative features through AdminJS. 

## How To Run Locally

Clone this repository and take the following steps: 

In Terminal: 

## Go To File
cd plateo-a-full-stack-e-commerce-store

type 'ls' to see list of folders and packages. 



## Run Client
1. cd client
2. npm install
3. npm start

## Run Server
1. cd server
2. npm install
3. node index.js




