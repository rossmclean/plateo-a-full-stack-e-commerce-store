

////////////////-----------------ADMINBRO----------------------///////////////////

// GENERAL CONFIG
const express = require('express');
const app = express();
require('dotenv').config();
const port = process.env.PORT || 5000;


const cors = require("cors");
app.use(cors())

// CONNECTING TO DB
const mongoose = require('mongoose');
(async function () {
  try {
    await mongoose.connect('mongodb+srv://main-user:main-user@ecommapp.oymhb.mongodb.net/?retryWrites=true&w=majority', {
      useUnifiedTopology: true,
      useNewUrlParser: true
    });
    console.log('Your DB is running');
  } catch (error) {
    console.log('your DB is not running. Start it up!');
  }
})()

// ADMINJS
const AdminJS = require('adminjs');
AdminJS.registerAdapter(require('@adminjs/mongoose'));
const Categories = require('./models/categories.model'); 
const Products = require('./models/products.model'); 
const Members = require('./models/members.model');
const UsersAdmin = require('./admin/resource_options/users.admin'); 

const buildAdminRouter = require('./admin/admin.router');

const admin = new AdminJS({
  resources: [Categories, Products, UsersAdmin, Members] 
});
const router = buildAdminRouter(admin);
app.use(admin.options.rootPath, router);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//* =============   ROUTES   ================== *//
app.use('/server/users', require('./routes/users.route'));
app.use('/server/categories', require('./routes/categories.route'));
app.use('/server/products', require('./routes/products.route'));
app.use('/server/members', require('./routes/members.route'))
app.use('/server/payment', require('./routes/payments.route'))
app.use('/server/emails', require('./routes/emails.route'))

//* =============   DEPLOY!   ================== *//

const path = require('path');

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});



app.listen(port, () => console.log(`🚀 Server running on port ${port} 🚀`));


