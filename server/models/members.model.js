const mongoose = require("mongoose");

const membersSchema = new mongoose.Schema({
    email: { type: String, unique: true, required: true },
	password: { type: String, required: true }
},

{strictQuery: false}
)

module.exports = mongoose.model("members", membersSchema);