const mongoose = require("mongoose");

const categoriesSchema = new mongoose.Schema({
    category: { type: String, required: true, unique: true},
    imgurl: { type: String, required: true, unique: true},
    description: { type: String, required: true, unique: true},
    
});

module.exports = mongoose.model("categories", categoriesSchema)