const mongoose = require("mongoose");

const productsSchema = new mongoose.Schema({
    category: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: "categories",
      },
    product: 
    {name: {type: String, required: true, unique: true},
	price: {type: Number, required: true},
	color: {type: String, required: true},
  img: {type: String, unique: true, required: true},
	description: {type: String, required: true, unique: true}}

});

module.exports = mongoose.model("products", productsSchema)