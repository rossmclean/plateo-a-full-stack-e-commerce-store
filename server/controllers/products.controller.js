const Products = require("../models/products.model");

const getAllProducts = async (req, res) => {
  try {
    const products = await Products.find({});
    res.send(products);
  } catch (e) {
    res.send({ e });
  }
};

const getOne = async (req, res) => {
  let { _id } = req.params;
  try {
    const product = await Products.findOne({ _id });
    res.send(product);
  } catch (e) {
    res.send({ e });
  }
};

module.exports = {
  getAllProducts,
  getOne,
};
