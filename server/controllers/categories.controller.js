const Categories = require("../models/categories.model");

const getAllCategories = async (req, res) => {
  try {
    const categories = await Categories.find({});
    res.send(categories);
  } catch (e) {
    res.send({ e });
  }
};

const getOne = async (req, res) => {
  let { _id } = req.params;
  try {
    const category = await Categories.findOne({ _id });
    res.send(category);
  } catch (e) {
    res.send({ e });
  }
};

module.exports = {
  getAllCategories,
  getOne,
};
