const nodemailer = require('nodemailer');

const transport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: process.env.NODEMAILER_USER ,
        pass: process.env.NODEMAILER_PASSWORD ,
    }
})

const send_email = async (req, res) => {

    const {name, email, message, subject } = req.body;
    const mailOptions = {
        to: process.env.NODEMAILER_USER,
        replyTo: email,
        subject: 'New Message from ' + name,
        html: `${message}
        <p>
        <pre></pre>
        </p>`
    };

    try {
        await transport.sendMail(mailOptions)
        console.log('===> Email sent!')
        return res.json({ok: true, message: 'email sent'})
        
    } catch (error) { 
        return res.json({ok: false, message: 'something is wrong', err})
        
    }

}

module.exports = {send_email}