const argon2 = require("argon2");
const Users = require("../models/users.model");

const register = async (req, res) => {
  try {
    const { email, password, admin } = req.body;
    const hash = await argon2.hash(password);
    const newUser = await Users.create({
      email,
      encryptedPassword: hash,
      admin: admin || false,
    });
    res.send(newUser);
  } catch (error) {
    res.send({ error });
    console.log("error ===>", error);
  }
};

module.exports = {
  register,
};
