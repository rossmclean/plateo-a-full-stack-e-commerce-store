const express = require('express');
const router = express.Router();
const controller = require('../controllers/members.controller');

router.post('/register', controller.register);
router.post('/login', controller.login);
router.post('/verify_token', controller.verify_token);


module.exports = router;