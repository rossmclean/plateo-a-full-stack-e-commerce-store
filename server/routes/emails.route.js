const express = require('express');
const router = express.Router();
const controller = require('../controllers/emails.controller');

router.post("/send_email", controller.send_email);

module.exports = router;