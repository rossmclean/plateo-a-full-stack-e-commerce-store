const express = require("express");
const router = express.Router();
const controller = require("../controllers/products.controller");

router.get("/", controller.getAllProducts);
router.get("/:_id", controller.getOne);


module.exports = router;
