const express = require("express");
const router = express.Router();
const controller = require("../controllers/categories.controller");

router.get("/", controller.getAllCategories);
router.get("/:_id", controller.getOne);

module.exports = router;
