const AdminJS = require("adminjs");
const AdminJSMongoose = require("adminjs-mongoose");
const Categories = require("../models/categories.model"); // change the path for yours
const Products = require("../models/products.model"); // change the path for yours
const Users = require("../models/users.model"); // change the path for yours

AdminJS.registerAdapter(AdminJSMongoose);

const options = {
       resources: [Categories, Products, Users], // the models must be included in this array
};

module.exports = options;
