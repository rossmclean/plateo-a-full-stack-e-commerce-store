const URL = window.location.hostname === `localhost`
            ? `http://localhost:5000/server`
            : `https://salty-stream-22014.herokuapp.com/server`
            
export { URL }